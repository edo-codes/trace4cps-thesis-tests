
package codes.edo.trace4cps.test.gen;

import org.eclipse.trace4cps.analysis.mtl.impl.AbstractMTLformula;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLnext;

import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

/*
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

public class MTLnextGen extends Generator<MTLnext> {
    public MTLnextGen() {
        super(MTLnext.class);
    }

    @Override
    public MTLnext generate(SourceOfRandomness random, GenerationStatus status) {
        boolean allowTemporal = status.valueOf(MtlFormulaGen.allowTemporalKey).get();
        boolean allowNext = status.valueOf(MtlFormulaGen.allowNextKey).get();
        if (status.valueOf(MtlFormulaGen.allowNestedKey).get() == false) {
            status.setValue(MtlFormulaGen.allowTemporalKey, false);
        }
        if (status.valueOf(MtlFormulaGen.allowNestedNextKey).get() == false) {
            status.setValue(MtlFormulaGen.allowNextKey, false);
            //temp
            status.setValue(MtlFormulaGen.allowTemporalKey, false);
        }
        AbstractMTLformula sub = gen().make(AbstractMTLformulaGen.class).generate(random, status);
        status.setValue(MtlFormulaGen.allowNextKey, allowNext);
        status.setValue(MtlFormulaGen.allowTemporalKey, allowTemporal);
        return new MTLnext(sub);
    }
}
