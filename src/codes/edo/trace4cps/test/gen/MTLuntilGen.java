
package codes.edo.trace4cps.test.gen;

import org.eclipse.trace4cps.analysis.mtl.impl.AbstractMTLformula;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLuntil;
import org.eclipse.trace4cps.core.impl.Interval;

import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

/*
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

public class MTLuntilGen extends Generator<MTLuntil> {
    public MTLuntilGen() {
        super(MTLuntil.class);
    }

    @Override
    public MTLuntil generate(SourceOfRandomness random, GenerationStatus status) {
        Generator<AbstractMTLformula> sub = gen().make(AbstractMTLformulaGen.class);
        Interval i = gen().make(IntervalGen.class).generate(random, status);
        boolean allowTemporal = status.valueOf(MtlFormulaGen.allowTemporalKey).get();
        if (status.valueOf(MtlFormulaGen.allowNestedKey).get() == false) {
            status.setValue(MtlFormulaGen.allowTemporalKey, false);
        }
        AbstractMTLformula left = sub.generate(random, status);
        AbstractMTLformula right = sub.generate(random, status);
        status.setValue(MtlFormulaGen.allowTemporalKey, allowTemporal);
        return new MTLuntil(left, right, i);
    }
}
